"""
urls.py
"""

from django.contrib import admin
from django.urls import include, re_path, path
from rest_framework import routers
from wishlist import views

app_name = "api"

ROUTER = routers.DefaultRouter()
ROUTER.register(r"persons", views.PersonsListViewSet, base_name="PersonsView")
ROUTER.register(r"wishes", views.WishesListViewSet, base_name="WishesView")

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    re_path(r"^", include(ROUTER.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("admin/", admin.site.urls),
]
urlpatterns += ROUTER.urls
