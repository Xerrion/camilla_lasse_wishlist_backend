"""
Serializers
"""

from rest_framework import serializers
from .models import Person, Wish


class PersonSerializer(serializers.ModelSerializer):
    """
    Here we specify what field we want to show, and then serialize the data
    """

    class Meta:
        model = Person
        fields = ("id", "first_name", "last_name")


class WishSerializer(serializers.ModelSerializer):
    """
    Here we specify what field we want to show, and then serialize the data
    """

    class Meta:
        model = Wish
        fields = ("id", "item_name", "description", "website", "price", "wisher_id")
