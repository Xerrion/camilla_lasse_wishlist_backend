"""
models.py
"""
from django.db import models
from django.utils.translation import gettext_lazy as _


class Person(models.Model):
    """
    Our person model, has a first and last name
    """

    first_name: str = models.CharField(max_length=100)
    last_name: str = models.CharField(max_length=100)

    def __str__(self) -> str:
        full_name: str = f"{self.first_name} {self.last_name}"
        return full_name

    class Meta:
        verbose_name_plural = _("Persons")
        verbose_name = _("Person")


class Wish(models.Model):
    """
    Our wish model, which can be created with a POST request
    """

    item_name: str = models.CharField(max_length=100, verbose_name=_("Item Name"))
    description: str = models.TextField(verbose_name=_("Description"))
    website: str = models.URLField(blank=True, null=True, verbose_name=_("Website"))
    price: int = models.IntegerField(verbose_name=_("Price"))
    wisher_id: Person = models.ForeignKey(
        Person, on_delete=models.CASCADE, verbose_name=_("wisher")
    )

    def __str__(self) -> str:
        item_name: str = f"{self.item_name}"
        return item_name

    class Meta:
        verbose_name_plural = _("Wishes")
        verbose_name = _("Wish")
