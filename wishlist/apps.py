"""
apps.py
"""
from django.apps import AppConfig


class WishlistConfig(AppConfig):
    """
    App configuration
    """

    name = "wishlist"
