"""
Views.py
"""

from rest_framework import viewsets, mixins
from .models import Person, Wish
from .serializers import PersonSerializer, WishSerializer


class PersonsListViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    """
    Get data about our persons
    """

    queryset = Person.objects.all().order_by("-first_name")
    serializer_class = PersonSerializer


class WishesListViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """
    Get data about all wishes
    """

    queryset = Wish.objects.all().order_by("-item_name")
    serializer_class = WishSerializer
