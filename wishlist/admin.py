"""
admin.py
"""
from django.contrib import admin
from .models import Person, Wish

admin.site.register(Person)
admin.site.register(Wish)
